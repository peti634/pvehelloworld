
function $(id) {
    return document.getElementById(id);
}


function BlockType(id, name, url, objname, csoport, csindex, leiras, tul) {
    this.id = id;
    this.name = name;
    this.url = url;
    this.objname = objname;
    this.csoport = csoport;
    this.csoportindex = csindex;
    this.leiras = leiras;
    this.tul = tul;
    this.taxatlas_index = -1;//csak textúra atlas használatakor, atlas helyét mutatja majd meg
}
function CsoportType(id, name) {
    this.id = id;
    this.name = name;
    this.elemek = [];
    this.tul = {
        fenyblockade: 0,
        vizalpha: 0,
        texatlas: 0,
        obj3d: 0,
        pontfeny_rgb: 0,
        szabadfeny: 0,
    };
}


var BlockTypeList = [];
var CsoportTypeList = [];

function NewCsoportType(id, name, tullist) {
    CsoportTypeList[id] = new CsoportType(id, name);
    for (var i = 0; i < tullist.length; i++) {
        CsoportTypeList[id].tul[tullist[i]] = 1;
    }
}
function AddCsoportElem(csid, bid) {
    var cselem = CsoportTypeList[csid];
    return cselem.elemek.push(bid) - 1;

}
function NewBlockType(id, name, url, objname, csoport, leiras) {
    var ret = AddCsoportElem(csoport, id);
    BlockTypeList[id] = new BlockType(id, name, url, objname, csoport, ret, leiras, CsoportTypeList[csoport].tul);

}

NewCsoportType(1, "alapterrain", ["texatlas","fenyblockade"]);
NewCsoportType(2, "víz", ["vizalpha", "texatlas","szabadfeny"]);
NewCsoportType(3, "láva", ["vizalpha", "texatlas","szabadfeny"]);
NewCsoportType(10, "fény", ["obj3d","pontfeny_rgb","szabadfeny"]);

NewBlockType(1, "terep fű","ground_grass_gen_05.png", "", 1, "Fű");
NewBlockType(2, "kő","rock_3.png", "", 1, "Kő");
NewBlockType(3, "Kikövezett út","_paving2.png", "", 1, "Kikövezett út");
NewBlockType(4, "Csempe","_paving6.png", "", 1, "???");
NewBlockType(5, "Fa léc","_planks.png", "", 1, "???");
NewBlockType(6, "kő 2","_rock6.png", "", 1, "???");
NewBlockType(7, "kő fal","_stonewall4.png", "", 1, "???");
NewBlockType(8, "kő fal 2","_stonewall5.png", "", 1, "???");
NewBlockType(9, "kő fal 3","_stonewall6.png", "", 1, "???");
NewBlockType(10, "kő fal 4","_stonewall8.png", "", 1, "???");
NewBlockType(11, "Fa léc 2","_woodfloor1.png", "", 1, "???");
NewBlockType(12, "Fa törsz","_vegetation_tree_bark_40.png", "", 1, "???");

NewBlockType(13, "víz","water.png", "", 2, "???");

NewBlockType(14, "láva","lava.png", "", 3, "???");

NewBlockType(100, "RGB Pont fényforrás","", "Geo_Feny", 10, "???");



var INPUTKEYS = {
    KEY_W: 87,
    KEY_A: 65,
    KEY_S: 83,
    KEY_D: 68,
    KEY_0: 0x30,
    KEY_1: 0x31,
    KEY_2: 0x32,
    KEY_3: 0x33,
    KEY_4: 0x34,
    KEY_5: 0x35,
    KEY_Q: 81,
    KEY_T: 84,
    KEY_E: 0x45,
    KEY_SPACE: 32,
    KEY_CTRL: 17,
    KEY_SHIFT: 16,
    LBUTTON: 0,
    MBUTTON: 1,
    RBUTTON: 2
};

var pve;
var DEBUG = true;
var gl_;
var canv;
var tm_canv;
var ctx;

var napfeny = 1.0;

var icontrol;
var Box;
var vilag;
var terrain;
var terrain2;
var kijelol = {x: 0, y: 0};
var voxelkijelol = [0, 0, 0, -1];
var ObjSzerk;
var Range = 10;
var vvilag;

var box;
var plain;
var plain_tc;

var Voxelraypickbox;
var VoxelEpitTipus = 0;
var showvoxel = false;

var ViszonyPont;

var TerepSzerkTipus = 0;
var tatlas;

var kij_pos = [0, 0, 0];
var kij_mer = [1, 1, 1];

var mopos = {x: 0, y: 0};

function Start() {
    var utvonal_prefix = "tex/";

    canv = $("glcanvas");
    pve = new PokolVoxelEngine();
    var sett = new PokolVoxelEngine_DefaultSettings();
    sett.canvas = canv;

    sett.render_maxrange = 500;
    sett.voxel_napfenyRange = 4;
    sett.voxel_pontfenyMaxRange = 100;
    sett.voxel_blokSsize = 0.25;
    sett.voxel_blokkOldalFeny = [0.78, 0.7, 0.4, 1.0, 0.6, 0.85];
    sett.voxel_terrainLengthXZ = 30;
    sett.blokkTypeList = BlockTypeList;
    sett.blokkCsoportList = CsoportTypeList;
    sett.url_prefix = utvonal_prefix;


    sett.callback_draw = frameTick;

    gl_ = pve.initGL(sett);

    if (gl_) {

        tatlas = pve.getTerrainAtlas();

        $("startbut").style.display = "none";


        icontrol = pve.inputControl(Mouse_CB, undefined, Key_CB);
        icontrol.LoadInputKey(INPUTKEYS);
        icontrol.SetPos(-2, 2, -2);

        icontrol.SetNezszo(10, 130);



        vvilag = pve.voxelVilag;
        for (var i = 0; i < 8; i++) {
            for (var j = 0; j < 8; j++) {
                vvilag.addTerrain(i, j);

            }
        }

        for (var i = 0; i < 8; i++) {
            for (var j = 0; j < 8; j++) {
                vvilag.addElem(i, 0, j, 1, undefined);
            }
        }

        vvilag.addElem(1, 1, 1, 2, undefined);
        vvilag.update();


        var tolpos = [-vvilag.blocksize / 2, 0, -vvilag.blocksize / 2];
        vvilag.setRelativePos(tolpos);
        ViszonyPont = new pve.geos.Geo_Box([0.2, 0.2, 0.2], [0, 2, 0], [0, 0, 0]);
        pve.geos.addList(ViszonyPont);
        ViszonyPont.yheight = 0;
        ViszonyPont.fadecolor = 0;

        pve.start();
        setInterval("kiirFPS()", 1000);



    }
}

function Mouse_CB(e, movm, mpos) {

    if (GetPointerLock()) {
        if (icontrol.key(INPUTKEYS.RBUTTON) && TerepSzerkTipus === 0)
            return;
        icontrol.nezszog[0] += movm.y / 10.0;
        icontrol.nezszog[1] += movm.x / 10.0;


        mopos.x = canv.width >> 1;
        mopos.y = canv.height >> 1;

    } else {
        mopos = mpos;
    }
}


function Key_CB(key, up, dbclick) {
    if (up)
        return;
    if (key === INPUTKEYS.LBUTTON) {
        icontrol.MouseLock();
        return;
    }
    
    if (icontrol.key(INPUTKEYS.KEY_CTRL) && icontrol.key(INPUTKEYS.KEY_S)) {
        return true;
    }
}

function frameDraw() {


        pve.clear([0.5, 0.5, 0.8, 1.0]);
    

    pve.updateCamera(icontrol, [mopos.x, mopos.y]);




    gl_.enable(gl_.CULL_FACE);


    gl_.mvPush();

    pve.napfeny = napfeny;
    vvilag.Draw(true);



    gl_.mvPop();


    gl_.disable(gl_.CULL_FACE);

}

function KeyTick() {
    icontrol.start();
    var speed = 0.05;
    if (icontrol.key(INPUTKEYS.KEY_SHIFT))
        speed = 0.5;
    if (icontrol.key(INPUTKEYS.KEY_W)) {
        icontrol.MoveCam(icontrol.nezszog[0], icontrol.nezszog[1], speed);
    }
    if (icontrol.key(INPUTKEYS.KEY_S)) {
        icontrol.MoveCam(icontrol.nezszog[0], icontrol.nezszog[1], -speed);
    }
    if (icontrol.key(INPUTKEYS.KEY_D)) {
        icontrol.MoveCam(0, icontrol.nezszog[1] + 90, speed);
    }
    if (icontrol.key(INPUTKEYS.KEY_A)) {
        icontrol.MoveCam(0, icontrol.nezszog[1] - 90, speed);
    }
    if (icontrol.key(INPUTKEYS.KEY_SPACE)) {
        icontrol.MoveCam(-90, 0, speed);
    }
    if (icontrol.key(INPUTKEYS.KEY_CTRL)) {
        icontrol.MoveCam(90, 0, speed);
    }
    icontrol.end();
}

var fps = 0;
var frametime;
var lowframetime;

function frameTick() {
    var akttime = performance.now();
    if (lowframetime === 0 || lowframetime < (akttime - frametime))
        lowframetime = akttime - frametime;
    frametime = akttime;

    fps++;


    KeyTick();
    frameDraw();


    ViszonyPont.fadecolor++;
    ViszonyPont.fadecolor %= 100;


    var color = ViszonyPont.fadecolor / 50;
    color = (color > 1) ? (2 - color) : (color);
    ViszonyPont.setColor( [color, color, color]);

}
function kiirFPS() {
    var perfstr = (((lowframetime * 1000) | 0) / 1000);
    $("fps").innerHTML = "FPS: " + fps + "<BR>usec:" + perfstr;
    lowframetime = 0;
    fps = 0;
    osszdraw = 0;
}
